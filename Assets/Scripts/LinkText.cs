﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class LinkText : MonoBehaviour,IPointerClickHandler {

    public Dropdown TargetDropdown;

    void Start()
    {

    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (TargetDropdown.value > -1)
        {
            string url;
            switch (TargetDropdown.captionText.text)
            {
                case "ゆうパック":
                    url = "http://www.post.japanpost.jp/smt-simulator/youpack.php";
                    break;
                case "クロネコヤマト":
                    url = "http://www.kuronekoyamato.co.jp/ytc/search/estimate/ichiran.html";
                    break;
                case "佐川急便":
                    url = "http://www.sagawa-exp.co.jp/send/fare/list/sagawa_faretable/";
                    break;
                default:
                    url = "";
                    break;
            }

            if(url != "")
            {
                Debug.Log("ブラウザで開く:" + url);
                Application.OpenURL(url);
            }
        }
    }
}
