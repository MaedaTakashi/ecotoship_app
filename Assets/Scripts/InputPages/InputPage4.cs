﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputPage4 : InputPageBase
{

    private Dictionary<string, Toggle> items;

    void Start () {
        Transform tranToggles = this.transform.Find("Toggles");

        GameObject goOrg = tranToggles.Find("ItemToggle").gameObject;
        Vector3 posOrg = goOrg.GetComponent<RectTransform>().anchoredPosition;

        List<string> listItemStr = new List<string>();
        listItemStr.Add("ぬいぐるみ");
        listItemStr.Add("おもちゃ");
        listItemStr.Add("衣類");
        listItemStr.Add("服飾雑貨");
        listItemStr.Add("ベビー用品");
        listItemStr.Add("食器");
        listItemStr.Add("キッチン用品");
        listItemStr.Add("楽器・音響機器");
        listItemStr.Add("調度品");
        listItemStr.Add("アウトドア");
        listItemStr.Add("スポーツ用品");
        listItemStr.Add("自転車");
        listItemStr.Add("家電");
        listItemStr.Add("家具");
        listItemStr.Add("工具・農機具");
        listItemStr.Add("その他商品");

        items = new Dictionary<string, Toggle>();

        for (int i = 0; i < listItemStr.Count; i++)
        {
            GameObject go = GameObject.Instantiate(goOrg, tranToggles);
            Vector3 pos = posOrg;
            int row = i / 2;
            int col = i % 2;
            pos.x += col * 220;
            pos.y += row * -60;
            go.GetComponent<RectTransform>().anchoredPosition = pos;
            go.name = "item" + i.ToString("00");
            go.transform.Find("Label").GetComponent<Text>().text = listItemStr[i];

            items.Add(listItemStr[i],go.GetComponent<Toggle>());
        }


        foreach(string str in UserInputData.Data.item)
        {
            Toggle tgl;
            if(items.TryGetValue(str, out tgl))
            {
                tgl.isOn = true;
            }
        }
    }

    public override void ClickNext()
    {
        UserInputData.Data.item.Clear();

        foreach (KeyValuePair<string,Toggle> pair in items)
        {
            if (pair.Value.isOn)
            {
                UserInputData.Data.item.Add(pair.Key);
            }
        }

        List<string> errStr = new List<string>();
        if (UserInputData.Data.item.Count == 0)
        {
            errStr.Add("品物 が選択されていません。");
        }

        if (errStr.Count > 0)
        {
            AppManager.Instance.ErrDialog.Show(string.Join("\n", errStr.ToArray()));
            return;
        }

        UserInputData.Data.SaveData();
        base.ClickNext();
    }
}
