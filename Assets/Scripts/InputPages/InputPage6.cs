﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Text;

public class InputPage6 : InputPageBase
{
    [SerializeField]
    private InputField txtMsg;

    void Start () {
    }

    private Coroutine coroutine;

    public override void ClickNext()
    {
        UserInputData.Data.message = txtMsg.text;
        if (coroutine != null) return;
        coroutine = StartCoroutine(coWebRequest());
    }

    private IEnumerator coWebRequest()
    {
        string url = "https://eco-to-ship.jp/app_mail.php";

        //フォームデータ生成
        WWWForm form = new WWWForm();
        form.AddField("name", UserInputData.Data.your_name);
        form.AddField("name_kana", UserInputData.Data.your_name_kana);
        form.AddField("your_individual_or_corporate", UserInputData.Data.your_individual_or_corporate);
        form.AddField("your_company", UserInputData.Data.your_company);
        form.AddField("zip", UserInputData.Data.zip);
        form.AddField("addr", UserInputData.Data.addr);
        form.AddField("your_tel", UserInputData.Data.your_tel);
        form.AddField("your_mail", UserInputData.Data.your_email);
        //form.AddField("nengo", UserInputData.Data.nengo);
        //form.AddField("year", UserInputData.Data.year);
        //form.AddField("month", UserInputData.Data.month);
        //form.AddField("day", UserInputData.Data.day);
        form.AddField("gender", UserInputData.Data.gender);

        string[] itemAry = UserInputData.Data.item.ToArray();
        form.AddField("item", string.Join(",", itemAry));

        form.AddField("box", UserInputData.Data.box);
        form.AddField("lc", UserInputData.Data.lc);
        form.AddField("donations", UserInputData.Data.donations);
        form.AddField("introduction", UserInputData.Data.introduction);
        form.AddField("message", UserInputData.Data.message);
        

        using (var w = UnityWebRequest.Post(url, form))
        {
            w.chunkedTransfer = false;

            yield return w.SendWebRequest();
            if (w.isNetworkError || w.isHttpError)
            {
                Debug.Log("エラー");
                Debug.Log(w.error);
                Debug.Log("responseCode:" + w.responseCode);
                Debug.Log(w.downloadHandler.text);
                AppManager.Instance.ErrDialog.Show("送信エラー コード:" + w.responseCode + "\n"
                    + w.error + "\n"
                    + w.downloadHandler + "\n");
            }
            else
            {
                Debug.Log("Req OK");
                Debug.Log("responseCode:" + w.responseCode);
                Debug.Log(w.downloadHandler.text);
                gotoNextPanel();
            }
        }
        
        coroutine = null;
    }

    private void gotoNextPanel()
    {
        this.transform.parent.gameObject.SetActive(false);
        this.transform.parent.parent.Find("EndPage").gameObject.SetActive(true);
    }

}
