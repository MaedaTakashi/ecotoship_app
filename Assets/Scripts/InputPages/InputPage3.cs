﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;

public class InputPage3 : InputPageBase
{
    public InputField zip;
    public InputField addr;
    public InputField your_tel;
    public InputField your_email;

    void Start () {

        zip.text = UserInputData.Data.zip;
        addr.text = UserInputData.Data.addr;
        your_tel.text = UserInputData.Data.your_tel;
        your_email.text = UserInputData.Data.your_email;

    }


    public override void ClickNext()
    {
        UserInputData.Data.zip = zip.text.Trim();
        UserInputData.Data.addr = addr.text.Trim();
        UserInputData.Data.your_tel = your_tel.text.Trim();
        UserInputData.Data.your_email = your_email.text.Trim();

        List<string> errStr = new List<string>();
        if (UserInputData.Data.zip == "")
        {
            errStr.Add("郵便番号 が未入力です。");
        }
        else if (!IsZipcode(UserInputData.Data.zip))
        {
            errStr.Add("郵便番号 は7桁の半角数字で\n入力して下さい。");
        }

        UserInputData.Data.addr = addr.text;
        if (UserInputData.Data.addr == "")
        {
            errStr.Add("住所 が未入力です。");
        }

        UserInputData.Data.your_tel = your_tel.text;
        if (UserInputData.Data.your_tel == "")
        {
            errStr.Add("電話番号 が未入力です。");
        }
        else if (!IsTelno(UserInputData.Data.your_tel))
        {
            errStr.Add("電話番号 の書式が不正です。");
        }

        UserInputData.Data.your_email = your_email.text;
        if (UserInputData.Data.your_email == "")
        {
            errStr.Add("メールアドレス が未入力です。");
        }
        else if (!IsMailAddress(UserInputData.Data.your_email))
        {
            errStr.Add("メールアドレスの書式が\n不正です。");
        }

        if (errStr.Count > 0)
        {
            AppManager.Instance.ErrDialog.Show(string.Join("\n", errStr.ToArray()));
            return;
        }

        UserInputData.Data.SaveData();
        base.ClickNext();
    }

    /// <summary>
    /// 指定された文字列が郵便番号かどうか
    /// </summary>
    private bool IsZipcode(string input)
    {
        if (string.IsNullOrEmpty(input))
        {
            return false;
        }
        return Regex.IsMatch(
            input,
            @"^[0-9]{7}$",
            RegexOptions.IgnoreCase
        );
    }

    /// <summary>
    /// 指定された文字列が電話番号かどうか
    /// </summary>
    private bool IsTelno(string input)
    {
        if (string.IsNullOrEmpty(input))
        {
            return false;
        }
        return Regex.IsMatch(
            input,
            @"^[0-9-()]{9,16}$",
            RegexOptions.IgnoreCase
        );
    }

    /// <summary>
    /// 指定された文字列がメールアドレスかどうか
    /// </summary>
    private bool IsMailAddress(string input)
    {
        if (string.IsNullOrEmpty(input))
        {
            return false;
        }
        return Regex.IsMatch(
            input,
            @"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$",
            RegexOptions.IgnoreCase
        );
    }
}
