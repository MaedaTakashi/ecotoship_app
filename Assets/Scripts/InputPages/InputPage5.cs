﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;

public class InputPage5 : InputPageBase
{
    public Dropdown box;
    public ToggleGroup slip;
    public Dropdown post;
    public ToggleGroup lc;
    public Button nextBtn;
    public Button backBtn;

    private LC nearLC;

    void Start()
    {
        box.options.Clear();
        for(int i = 1; i < 21; i++)
        {
            box.options.Add(new Dropdown.OptionData(i.ToString()));
        }

        if (UserInputData.Data.box == "")
        {
            box.value = 0;
        }
        else
        { 
            box.value = CommonFunc.FindDropdownIndex(box, UserInputData.Data.box);
        }

        if (LC.Data != null)
        {
            List<string> listLc = new List<string>();
            foreach (LC lcData in LC.Data)
            {
                listLc.Add(lcData.GetNameAddr());
            }
            CommonFunc.CreateTggle(lc, listLc, 400f);
        }

        StartCoroutine(GetNearLC());
    }

    private IEnumerator GetNearLC()
    {
        LocationManager lm = AppManager.Instance.LocationManager;
        lm.StartGetLocation();
        yield return null;

        //1秒おきに位置情報マネージャを確認し取得出来たら次の処理へ
        //出来なかったか失敗したら処理を抜ける
        bool getLocOK = false;
        for (int i = 0; i < 10; i++)
        {
            if (lm.GetLocationProcStatus != 0)
            {
                //処理中以外のステータスになった
                if(lm.GetLocationProcStatus == 1)
                {
                    //成功
                    getLocOK = true;
                    break;
                }
                //成功以外
                break;
            }
            yield return new WaitForSeconds(1.0f);
        }
        if (!getLocOK)
        {
            procNo();
            yield break;
        }

        //Debug.Log("Loc lat:" + lm.Location.latitude + " lon:" + lm.Location.longitude);

        float lat = lm.Location.latitude;
        float lon = lm.Location.longitude;
        //float lat = 35.4f;
        //float lon = 135.9f;

        //至近のLCを取得
        nearLC = LC.GetNearLC(lat, lon);
        if (nearLC != null) {
            //AppManager.Instance.YesNoDialog.Show(nearLC.Name + "\n\n一番近い集荷先です。\nここに送りますか？", procYes, procNo);

            StringBuilder sb = new StringBuilder();
            //sb.Append("現在位置:");
            //sb.Append(lat);
            //sb.Append(",");
            //sb.Append(lon);
            //sb.Append("\n\n");

            sb.Append(nearLC.Name);
            sb.Append("\n");
            sb.Append("(");
            sb.Append(nearLC.Addr);
            sb.Append(")");
            sb.Append("\n\n");

            //sb.Append(nearLC.Lat);
            //sb.Append(",");
            //sb.Append(nearLC.Lon);
            //sb.Append("\n約");
            //sb.Append(nearLC.Distance / 1000f);
            //sb.Append("km\n");
            sb.Append("一番近い集荷先です。\nここに送りますか？");

            AppManager.Instance.YesNoDialog.Show(sb.ToString(), procYes, procNo);
        }
    }

    private void ShowNearLC()
    {
    }

    private void procYes()
    {
        //Debug.Log("yes:" + nearLC.GetNameAddr());
        CommonFunc.FindAndSetTggleForScroll(lc, nearLC.GetNameAddr());
        nextBtn.interactable = true;
        backBtn.interactable = true;
    }

    private void procNo()
    {
        nextBtn.interactable = true;
        backBtn.interactable = true;
        //Debug.Log("no:" + nearLC.GetNameAddr());
        CommonFunc.FindAndSetTggleForScroll(lc, UserInputData.Data.lc);
    }


    public void ChangeToggle()
    {
    }

    public override void ClickNext()
    {
        UserInputData.Data.box = box.captionText.text;

        UserInputData.Data.lc = CommonFunc.GetToggleGroupStr(lc);

        List<string> errStr = new List<string>();
        if (UserInputData.Data.box == "")
        {
            errStr.Add("箱数 が未入力です。");
        }

        if (UserInputData.Data.lc == "")
        {
            errStr.Add("送り先 が未入力です。");
        }

        if (errStr.Count > 0)
        {
            AppManager.Instance.ErrDialog.Show(string.Join("\n", errStr.ToArray()));
            return;
        }


        UserInputData.Data.SaveData();
        base.ClickNext();
    }
}
