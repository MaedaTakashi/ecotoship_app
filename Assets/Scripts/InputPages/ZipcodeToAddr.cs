﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class ZipcodeToAddr : MonoBehaviour
{
    public InputField Zipcode;
    public InputField Addr;
    
    [System.Serializable]
    public class ZipcouldData
    {
        public int status;
        public int message;
        public ZipcouldDataResults[] results;
    }

    [System.Serializable]
    public class ZipcouldDataResults
    {
        public string zipcode;
        public string prefcode;
        public string address1;
        public string address2;
        public string address3;
        public string kana1;
        public string kana2;
        public string kana3;
    }

    public void ZipcodeInputEnd()
    {
        string tmp = Addr.text.Trim();
        if (tmp != "") return;

        string zip = Zipcode.text.Trim();
        if (zip.Length != 7) return;
        int tmpInt;
        if (int.TryParse(zip,out tmpInt)){
            StartCoroutine(GetText(zip));
        }
    }

    IEnumerator GetText(string zipCodeStr)
    {
        UnityWebRequest request = UnityWebRequest.Get("http://zipcloud.ibsnet.co.jp/api/search?zipcode=" + zipCodeStr);

        //Debug.Log("startreq " + zipCodeStr);

        // リクエスト送信
        yield return request.SendWebRequest();

        // 通信エラーチェック
        if (request.isHttpError)
        {
            Debug.Log(request.error);
        }
        else if (request.isNetworkError)
        {
            Debug.Log(request.error);
        }
        else
        {
            if (request.responseCode == 200)
            {
                string text = request.downloadHandler.text;
                Debug.Log(text);
                ZipcouldData data = JsonUtility.FromJson<ZipcouldData>(text);

                if(data.status == 200)
                {
                    if (data.results.Length > 0)
                    {
                        ZipcouldDataResults res = data.results[0];
                        Addr.text = res.address1 + res.address2 + res.address3;
                    }
                }
            }
        }
    }

}
