﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class InputPage1 : InputPageBase
{
    public Dropdown donations;
    public ToggleGroup report;
    public ToggleGroup introduction;

    void Start () {
        donations.ClearOptions();
        donations.options.Add(new Dropdown.OptionData("公益社団法人 ハタチ基金"));
        donations.options.Add(new Dropdown.OptionData("特定非営利活動法人 神奈川子ども未来ファンド"));
        donations.options.Add(new Dropdown.OptionData("日本赤十字社"));
        donations.options.Add(new Dropdown.OptionData("横浜市社会福祉基金 子どもの貧困対策"));
        donations.options.Add(new Dropdown.OptionData("仙台ふるさと応援寄附 子育て支援"));
        donations.options.Add(new Dropdown.OptionData("一般社団法人 アスバシ"));
        donations.options.Add(new Dropdown.OptionData("埼玉県子ども食堂ネットワーク"));
        donations.options.Add(new Dropdown.OptionData("相模原市 子ども・若者未来基金"));
        donations.options.Add(new Dropdown.OptionData("京都市子ども若者はぐくみ事業基金"));
        
        donations.value = CommonFunc.FindDropdownIndex(donations, UserInputData.Data.donations);

        CommonFunc.FindAndSetTggle(introduction, UserInputData.Data.introduction);
    }


    public override void ClickNext()
    {
        List<string> errStr = new List<string>();

        UserInputData.Data.donations = donations.captionText.text;
        if (UserInputData.Data.donations == "")
        {
            errStr.Add("寄付先 が未入力です。");
        }
        UserInputData.Data.introduction = CommonFunc.GetToggleGroupStr(introduction);

        if (errStr.Count > 0)
        {
            AppManager.Instance.ErrDialog.Show(string.Join("\n",errStr.ToArray()));
            return;
        }

        UserInputData.Data.SaveData();
        base.ClickNext();
    }
    
}
