﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputPage2 : InputPageBase {

    public InputField your_Name;
    public InputField your_Name_kana;

    public ToggleGroup kojin_houjin;
    public InputField your_company;

    //public Dropdown nengo;
    //public InputField year;
    //public Dropdown month;
    //public Dropdown day;

    public Dropdown gender;

    void Start () {

        your_Name.text = UserInputData.Data.your_name;
        your_Name_kana.text = UserInputData.Data.your_name_kana;

        CommonFunc.FindAndSetTggle(kojin_houjin, UserInputData.Data.your_individual_or_corporate);
        your_company.text = UserInputData.Data.your_company;


        ////生年月日ドロップダウン準備
        //nengo.options.Clear();
        //nengo.options.Add(new Dropdown.OptionData("明治"));
        //nengo.options.Add(new Dropdown.OptionData("大正"));
        //nengo.options.Add(new Dropdown.OptionData("昭和"));
        //nengo.options.Add(new Dropdown.OptionData("平成"));
        //nengo.value = 2;

        //month.options.Clear();
        //for(int i = 1; i < 13; i++)
        //{
        //    month.options.Add(new Dropdown.OptionData(i.ToString()));
        //}

        //day.options.Clear();
        //for (int i = 1; i < 32; i++)
        //{
        //    day.options.Add(new Dropdown.OptionData(i.ToString()));
        //}

        //if (UserInputData.Data.nengo!="")
        //{
        //    nengo.value = CommonFunc.FindDropdownIndex(nengo, UserInputData.Data.nengo);
        //}
        //year.text = UserInputData.Data.year;
        //month.value = CommonFunc.FindDropdownIndex(month, UserInputData.Data.month);
        //day.value = CommonFunc.FindDropdownIndex(day, UserInputData.Data.day);

        gender.value = CommonFunc.FindDropdownIndex(gender, UserInputData.Data.gender);
    }

    public void ChangeKojin_Houjin()
    {
        your_company.interactable = (CommonFunc.GetToggleGroupStr(kojin_houjin) == "法人");
    }

    public override void ClickNext()
    {
        List<string> errStr = new List<string>();

        UserInputData.Data.your_name = your_Name.text.Trim();
        if (UserInputData.Data.your_name == "")
        {
            errStr.Add("お名前 が未入力です。");
        }

        UserInputData.Data.your_name_kana = your_Name_kana.text.Trim();
        if (UserInputData.Data.your_name_kana == "")
        {
            errStr.Add("フリガナ が未入力です。");
        }

        UserInputData.Data.your_individual_or_corporate = CommonFunc.GetToggleGroupStr(kojin_houjin);
        if(UserInputData.Data.your_individual_or_corporate == "法人")
        {
            UserInputData.Data.your_company = your_company.text.Trim();
            if (UserInputData.Data.your_company == "")
            {
                errStr.Add("法人名 が未入力です。");
            }
        }
        else
        {
            UserInputData.Data.your_company = "";
        }

        //UserInputData.Data.nengo = nengo.captionText.text;
        //if (UserInputData.Data.nengo == "")
        //{
        //    errStr.Add("年号 が未入力です。");
        //}

        //UserInputData.Data.year = year.text;
        //if (UserInputData.Data.year == "")
        //{
        //    errStr.Add("生年 が未入力です。");
        //}

        //UserInputData.Data.month = month.captionText.text;
        //if (UserInputData.Data.month == "")
        //{
        //    errStr.Add("生月 が未入力です。");
        //}

        //UserInputData.Data.day = day.captionText.text;
        //if (UserInputData.Data.day == "")
        //{
        //    errStr.Add("生日 が未入力です。");
        //}

        UserInputData.Data.gender = gender.captionText.text;
        if (UserInputData.Data.gender == "")
        {
            errStr.Add("性別 が未入力です。");
        }

        if (errStr.Count > 0)
        {
            AppManager.Instance.ErrDialog.Show(string.Join("\n", errStr.ToArray()));
            return;
        }

        UserInputData.Data.SaveData();
        base.ClickNext();
    }

}
