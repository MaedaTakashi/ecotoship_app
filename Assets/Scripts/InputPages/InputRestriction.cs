﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Text.RegularExpressions;

public class InputRestriction : MonoBehaviour
{

    InputField inputField;
    Text text;
    string lastStr;
    void Start()
    {
        inputField = this.GetComponent<InputField>();
        text = this.transform.GetComponentInChildren<Text>();
        inputField.text = "a";
    }

    public void Input(string str)
    {
        if (chackInputStr(str))
        {
            Debug.Log("true:" + str);
            lastStr = str;
        }
        else
        {
            Debug.Log("fa:" + str);
            text.text = lastStr;
        }
    }

    private bool chackInputStr(string str)
    {
        Regex myRegExp = new Regex("^[\\d\\-]+$");
        return (myRegExp.IsMatch(str));
    }
}
