﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputPageBase : MonoBehaviour {

    public int PageNo;

    public virtual void ClickNext()
    {
        if (PageNo == 6)
        {
            this.transform.parent.gameObject.SetActive(false);
            this.transform.parent.parent.Find("EndPage").gameObject.SetActive(true);
        }
        else
        {
            GoPage(PageNo + 1);
        }
    }

    public virtual void ClickBack()
    {
        if (PageNo == 1)
        {
            this.transform.parent.gameObject.SetActive(false);
            this.transform.parent.parent.Find("TopPage").gameObject.SetActive(true);
        }
        else
        {
            GoPage(PageNo - 1);
        }
    }

    private void GoPage(int page)
    {
        this.gameObject.SetActive(false);
        this.transform.parent.Find("page" + page.ToString()).gameObject.SetActive(true);
    }
}
