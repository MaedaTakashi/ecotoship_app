﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class CommonFunc
{
    public static int FindDropdownIndex(Dropdown d ,string str)
    {
        for (int i = 0; i < d.options.Count;i++)
        {
            if (d.options[i].text.Equals(str, System.StringComparison.Ordinal))
            {
                return i;
            }
        }
        return -1;
    }

    public static bool FindAndSetTggle(ToggleGroup tg, string str)
    {
        Toggle[] tgls = tg.GetComponentsInChildren<Toggle>();
        int i = 0;
        foreach (Toggle tgl in tgls)
        {
            i++;
            if (tgl.GetComponentInChildren<Text>().text.Equals(str, System.StringComparison.Ordinal))
            {
                tgl.isOn = true;
                return true;
            }
        }
        //Debug.Log(" " + i + " " + tg.ActiveToggles().ToString());
        return false;
    }

    public static bool FindAndSetTggleForScroll(ToggleGroup tg, string str)
    {
        float parentHeight = tg.transform.parent.parent.GetComponent<RectTransform>().sizeDelta.y;

        Toggle[] tgls = tg.GetComponentsInChildren<Toggle>();
        int i = 0;
        foreach (Toggle tgl in tgls)
        {
            string textStr = tgl.GetComponentInChildren<Text>().text;
            if (textStr.Equals(str, System.StringComparison.Ordinal))
            {
                tgl.isOn = true;
                RectTransform rectTran = tgl.GetComponent<RectTransform>();
                float tglY = rectTran.anchoredPosition.y * -1f;
                float tglH = rectTran.sizeDelta.y;
                float tglBtm = tglY + (tglH / 2f);
                if (tglBtm > parentHeight)
                {
                    tg.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, (tglBtm - parentHeight));
                }
                return true;
            }
            i++;
        }
        //Debug.Log(" " + i + " " + tg.ActiveToggles().ToString());
        return false;
    }


    /// <summary>
    /// 現在あるトグルグループの内容をディクショナリの内容にして生成し直す
    /// </summary>
    /// <param name="tg"></param>
    /// <param name="dic"></param>
    public static void CreateTggle(ToggleGroup tg,List<string>dic,float maxHeight)
    {
        tg.SetAllTogglesOff();


        //既存トグルグループの情報
        Transform tgTran = tg.transform;
        RectTransform tgRectTran = tgTran.GetComponent<RectTransform>();
        float tgHeight = tgRectTran.sizeDelta.y;
        int childCnt = tgTran.childCount;

        //一番目の子の情報
        Toggle firstChildTgl = tg.GetComponentInChildren<Toggle>();
        Transform firstChild = firstChildTgl.transform;
        RectTransform firstChildRectTran = firstChild.GetComponent<RectTransform>();
        GameObject firstChiltGO = firstChild.gameObject;
        firstChild.GetComponentInChildren<Text>().text = "";
        float height = firstChildRectTran.sizeDelta.y;
        float posX = firstChildRectTran.anchoredPosition.x;
        float firstPosY = firstChildRectTran.anchoredPosition.y;

        //上端下端の空白
        float firstPadding = (firstPosY * -1f) - (height / 2f);
        float endPadding = tgHeight - (firstPadding + (height * childCnt) + (3f * (childCnt - 1)));

        //一旦1番目の子以外を消す
        foreach (Transform child in tgTran)
        {
            if (child != firstChild)
            {
                GameObject.Destroy(child.gameObject);
            }
        }


        int cnt = 0;
        foreach(string kv in dic)
        {
            if(cnt == 0)
            {
            //Debug.Log(kv);
                firstChild.GetComponentInChildren<Text>().text = kv;
            }
            else
            {
                GameObject newGO = GameObject.Instantiate(firstChiltGO, tgTran);
                RectTransform newRT = newGO.GetComponent<RectTransform>();
                newRT.anchoredPosition = new Vector2(posX, firstPosY - (cnt * height + 3f));

                newGO.GetComponentInChildren<Text>().text = kv;
            }
            cnt++;
        }

        
        float tgH = firstPadding + (height * cnt) + (3f * (cnt - 1)) + endPadding;
        tgRectTran.sizeDelta = new Vector2(tgRectTran.sizeDelta.x
            , tgH);

        RectTransform ppRectTran = tgTran.parent.parent.GetComponent<RectTransform>();
        ppRectTran.sizeDelta = new Vector2(ppRectTran.sizeDelta.x
            , Mathf.Min(tgH+4,maxHeight));

    }

    public static string GetToggleGroupStr(ToggleGroup tg)
    {
        if (!tg.AnyTogglesOn()) return "";

        foreach (Toggle tgl in tg.ActiveToggles())
        {
            if (tgl.isOn)
            {
                return tgl.GetComponentInChildren<Text>().text;
            }
        }
        return "";
    }

}
