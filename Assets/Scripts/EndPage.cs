﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndPage : MonoBehaviour {

    void Start()
    {
    }

    public void ShowKiyaku()
    {
        this.transform.Find("KiyakuWindow").gameObject.SetActive(true);
    }

    public void ClickQuit(int mode)
    {
        if(mode == 1)
        {
            PlayerPrefs.DeleteAll();
        }

        this.transform.Find("EndPopUp").gameObject.SetActive(true);
    }

    public void ClickEnd()
    {
        Application.Quit();
    }

}
