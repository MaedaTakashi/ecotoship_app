﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TopPage : MonoBehaviour {

    public Toggle tglAgree;
    public Button btnStart;

    void Start () {
        ChangeAgree();
    }

    void Update () {
		
	}

    public void ShowKiyaku()
    {
        this.transform.Find("KiyakuWindow").gameObject.SetActive(true);
    }

    public void ClickInputStart()
    {
        if (tglAgree.isOn)
        {
            this.gameObject.SetActive(false);
            this.transform.parent.Find("InputPage").gameObject.SetActive(true);
        }
    }

    public void ChangeAgree()
    {
        btnStart.interactable = tglAgree.isOn;
    }
}
