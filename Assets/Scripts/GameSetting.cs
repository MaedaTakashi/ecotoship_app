﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSetting : MonoBehaviour
{
    const float SETTING_VER = 0.01f;

    #region Singleton
    private static GameSetting instance;
    public static GameSetting Instance
    {
        get
        {
            if (instance == null)
            {
                instance = (GameSetting)FindObjectOfType(typeof(GameSetting));
            }
            return instance;
        }
    }


    public void Awake()
    {
        if (this != Instance)
        {
            Debug.Log("GameSetting Destroy");
            Destroy(this.gameObject);
            return;
        }
        else
        {
            //DontDestroyOnLoad(gameObject);
            LoadSettingData();
        }
    }
    #endregion Singleton


    public void LoadSettingData()
    {
        UserInputData.Data = new UserInputData();
        UserInputData.Data.LoadData();
    }
}
