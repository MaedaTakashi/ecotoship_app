﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class ErrDialog : MonoBehaviour,IPointerClickHandler {

    public void Show(string str)
    {
        Text txtMsg = this.transform.Find("Dialog").Find("txt").GetComponent<Text>();
        txtMsg.text = str;
        this.gameObject.SetActive(true);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        this.gameObject.SetActive(false);
    }

}
