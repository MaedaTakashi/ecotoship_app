﻿using UnityEngine;
using System.Collections;

public class LocationManager : MonoBehaviour
{
    public LocationServiceStatus Status;
    public LocationInfo Location;

    private Coroutine getLocCor;

    /// <summary>
    /// 位置情報取得結果
    /// 0 取得処理中、1 成功、2 失敗（タイムアウト）、3 位置情報無効
    /// </summary>
    public int GetLocationProcStatus;

    public void StartGetLocation()
    {
        GetLocationProcStatus = 0;
        Input.location.Start();
        if (getLocCor != null)
        {
            StopCoroutine(getLocCor);
        }
        getLocCor = StartCoroutine(getLocation());
    }


    IEnumerator getLocation()
    {
        for(int i=0;i<10;i++)
        {
            this.Status = Input.location.status;
            if (Input.location.isEnabledByUser)
            {
                switch (this.Status)
                {
                    case LocationServiceStatus.Stopped:
                        Input.location.Start();
                        break;
                    case LocationServiceStatus.Running:
                        this.Location = Input.location.lastData;
                        GetLocationProcStatus = 1;
                        Input.location.Stop();
                        yield break;
                    case LocationServiceStatus.Failed:
                        GetLocationProcStatus = 3;
                        yield break;
                    default:
                        break;
                }
            }
            else
            {
                GetLocationProcStatus = 3;
                yield break;
            }
            yield return new WaitForSeconds(1.0f);
        }

        GetLocationProcStatus = 2;
    }
}