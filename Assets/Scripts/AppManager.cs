﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AppManager : MonoBehaviour {

    #region Singleton
    private static AppManager instance;
    public static AppManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = (AppManager)FindObjectOfType(typeof(AppManager));

                if (instance == null)
                {
                    //Debug.LogError(typeof(GameBattleManager) + "is nothing");
                }
            }

            return instance;
        }
    }

    public void Awake()
    {
        if (this != Instance)
        {
            Destroy(this.gameObject);
            return;
        }
    }

    void OnDestroy()
    {
        if (instance == this)
        {
            instance = null;
        }
    }
    #endregion Singleton

    public LocationManager LocationManager;
    public ErrDialog ErrDialog;
    public YesNoDialog YesNoDialog;

    void Start () {
        Transform tranCanvas = GameObject.Find("Canvas").transform;
        tranCanvas.Find("InputPage").gameObject.SetActive(false);
        tranCanvas.Find("EndPage").gameObject.SetActive(false);
        tranCanvas.Find("TopPage").gameObject.SetActive(true);

        GameObject goGSD = GameObject.Find("GetServerData");
        goGSD.GetComponent<GetServerData>().GetAndLoadServerData();
    }
}
