﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserInputData {

    public static UserInputData Data;

    public string your_name;
    public string your_name_kana;
    public string your_individual_or_corporate;
    public string your_company;

    public string zip;
    public string addr;
    public string your_tel;
    public string your_email;
    //public string nengo;
    //public string year;
    //public string month;
    //public string day;

    public string gender;
    public List<string> item;

    /// <summary>
    /// 箱数
    /// </summary>
    public string box;

    ///// <summary>
    ///// 送り状伝票
    ///// </summary>
    //public string slip;

    ///// <summary>
    ///// 利用する運送会社
    ///// </summary>
    //public string post;

    /// <summary>
    /// 送り先
    /// </summary>
    public string lc;

    /// <summary>
    /// 寄付先
    /// </summary>
    public string donations;

    ///// <summary>
    ///// 寄付の報告と御礼の連絡
    ///// </summary>
    //public string report;

    /// <summary>
    /// サイト内での到着報告
    /// </summary>
    public string introduction;

    /// <summary>
    /// メッセージ
    /// </summary>
    public string message;

    public void LoadData()
    {
        //Debug.Log("Load UserInputData");

        your_name = PlayerPrefs.GetString("your_name");
        your_name_kana = PlayerPrefs.GetString("your_name_kana");
        your_individual_or_corporate = PlayerPrefs.GetString("your_individual_or_corporate");
        your_company = PlayerPrefs.GetString("your_company");
        zip = PlayerPrefs.GetString("zip");
        addr = PlayerPrefs.GetString("addr");
        your_tel = PlayerPrefs.GetString("your_tel");
        your_email = PlayerPrefs.GetString("your_email");
        //nengo = PlayerPrefs.GetString("nengo");
        //year = PlayerPrefs.GetString("year");
        //month = PlayerPrefs.GetString("month");
        //day = PlayerPrefs.GetString("day");
        gender = PlayerPrefs.GetString("gender");
        box = PlayerPrefs.GetString("box");
        //slip = PlayerPrefs.GetString("slip");
        //post = PlayerPrefs.GetString("post");
        lc = PlayerPrefs.GetString("lc");

        item = StrToList(PlayerPrefs.GetString("item"));

        donations = PlayerPrefs.GetString("donations");
        //report = PlayerPrefs.GetString("report");
        introduction = PlayerPrefs.GetString("introduction");

        //Debug.Log("Load UserInputData End");
    }

    public void SaveData()
    {
        //Debug.Log("Save UserInputData");

        PlayerPrefs.SetString("your_name", your_name);
        PlayerPrefs.SetString("your_name_kana", your_name_kana);
        PlayerPrefs.SetString("your_individual_or_corporate", your_individual_or_corporate);
        PlayerPrefs.SetString("your_company", your_company);
        PlayerPrefs.SetString("zip", zip);
        PlayerPrefs.SetString("addr", addr);
        PlayerPrefs.SetString("your_tel", your_tel);
        PlayerPrefs.SetString("your_email", your_email);
        //PlayerPrefs.SetString("nengo", nengo);
        //PlayerPrefs.SetString("year", year);
        //PlayerPrefs.SetString("month", month);
        //PlayerPrefs.SetString("day", day);
        PlayerPrefs.SetString("gender", gender);
        PlayerPrefs.SetString("box", box);
        //PlayerPrefs.SetString("slip", slip);
        //PlayerPrefs.SetString("post", post);
        PlayerPrefs.SetString("lc", lc);

        //string itemsStr = ListToStr(item);
        //Debug.Log(itemsStr);
        PlayerPrefs.SetString("item", ListToStr(item));

        PlayerPrefs.SetString("donations", donations);
        //PlayerPrefs.SetString("report", report);
        PlayerPrefs.SetString("introduction", introduction);

        PlayerPrefs.Save();

        //Debug.Log("Save UserInputData End");
    }


    /// <summary>
    /// 文字列を,で分解してリストに格納
    /// </summary>
    /// <param name="list"></param>
    /// <param name="str"></param>
    public List<string> StrToList(string str)
    {
        List<string> list = new List<string>();
        string[] tmp = str.Split(',');
        foreach (string tmpStr in tmp)
        {
             list.Add(tmpStr);
        }
        return list;
    }

    /// <summary>
    /// int型のリストを,で繋げた文字列にして返す
    /// </summary>
    /// <param name="list"></param>
    /// <param name="str"></param>
    public string ListToStr(List<String> list)
    {
        if (list.Count == 0)
        {
            return "";
        }

        StringBuilder s = new StringBuilder(list[0]);
        for (int i = 1; i < list.Count; i++)
        {
            s.Append(",");
            s.Append(list[i]);
        }
        return s.ToString();
    }
}
