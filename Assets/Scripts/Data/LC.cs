﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class LC {

    public static List<LC> Data;

    public string LcCode;
    public string Name;
    public string Addr;
    public float Lat;
    public float Lon;

    public float Distance;

    public static void Add(string[] dataStr)
    {
        if(Data== null)
        {
            Data = new List<LC>();
        }

        if (dataStr.Length < 5)
        {
            //桁不足？
            return;
        }
        else if (dataStr[0] == "")
        {
            //桁不足？
            return;
        }
        LC newData = new LC();
        newData.setParam(dataStr);
        Data.Add(newData);
    }

    public string GetNameAddr()
    {
        return Name + "(" + Addr + ")";
    }

    private void setParam(string[] dataStr)
    {
        LcCode = dataStr[0];
        Name = dataStr[1];
        Addr = dataStr[2];
        Lat = parseFloat(dataStr[3], 0f);
        Lon = parseFloat(dataStr[4], 0f);
    }

    private float parseFloat(string str,float defaultValue)
    {
        float f;
        if (float.TryParse(str,out f))
        {
            return f;
        }
        return defaultValue;
    }

    /// <summary>
    /// ２地点間の距離(m)を求める
    /// ヒュベニの公式から求める
    /// </summary>
    /// <param name="lat">対象の緯度</param>
    /// <param name="lon">対象の経度</param>
    /// <param name="mode">測地系 true:世界 false:日本</param>
    /// <returns>距離(m)</returns>
    public float GetDistance(float lat, float lon ,bool mode=true)
    {
        // 緯度経度をラジアンに変換
        float radLat1 = Mathf.Deg2Rad * this.Lat; // 緯度１
        float radLon1 = Mathf.Deg2Rad * this.Lon; // 経度１
        float radLat2 = Mathf.Deg2Rad * lat; // 緯度２
        float radLon2 = Mathf.Deg2Rad * lon; // 経度２

        // 緯度差
        float radLatDiff = radLat1 - radLat2;

        // 経度差算
        float radLonDiff = radLon1 - radLon2;

        // 平均緯度
        float radLatAve = (radLat1 + radLat2) / 2.0f;

        // 測地系による値の違い
        double a = mode ? 6378137.0 : 6377397.155; // 赤道半径
        //double b = mode ? 6356752.314140356 : 6356078.963; // 極半径
        //$e2 = ($a*$a - $b*$b) / ($a*$a);
        double e2 = mode ? 0.00669438002301188 : 0.00667436061028297; // 第一離心率^2
        //$a1e2 = $a * (1 - $e2);
        double a1e2 = mode ? 6335439.32708317 : 6334832.10663254; // 赤道上の子午線曲率半径

        double sinLat = Math.Sin(radLatAve);
        double W2 = 1.0f - e2 * (sinLat * sinLat);
        double M = a1e2 / (Math.Sqrt(W2) * W2); // 子午線曲率半径M
        double N = a / Math.Sqrt(W2); // 卯酉線曲率半径

        double t1 = M * radLatDiff;
        double t2 = N* Math.Cos(radLatAve) * radLonDiff;
        double dist = Math.Sqrt((t1 * t1) + (t2 * t2));

        return (float)dist;
    }

    public void CalcDistance(float lat, float lon)
    {
        Distance = GetDistance(lat, lon);
    }


    public static LC GetNearLC(float lat, float lon)
    {
        if (Data == null) return null;
        if (Data.Count == 0) return null;
        LC nearLc = null;
        foreach(LC lc in Data)
        {
            lc.CalcDistance(lat, lon);
            if (nearLc == null)
            {
                nearLc = lc;
            }
            else
            {
                if(nearLc.Distance > lc.Distance)
                {
                    nearLc = lc;
                }
            }
        }
        return nearLc;
    }
}
