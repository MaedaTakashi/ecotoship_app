﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class GetServerData : MonoBehaviour
{
    public void GetAndLoadServerData()
    {
        StartCoroutine(GetData());
    }

    IEnumerator GetData()
    {
        UnityWebRequest request = UnityWebRequest.Get("https://eco-to-ship.jp/LC_Info.csv");

        // リクエスト送信
        yield return request.SendWebRequest();

        // 通信エラーチェック
        if (request.isHttpError)
        {
            Debug.Log(request.error);
        }
        else if (request.isNetworkError)
        {
            Debug.Log(request.error);
        }
        else
        {
            if (request.responseCode == 200)
            {
                string text = request.downloadHandler.text;
                //Debug.Log(text);
                string[] lines = text.Split('\n');
                foreach(string line in lines)
                {
                    string tmp = line.Trim();
                    if (tmp != "")
                    {
                        LC.Add(tmp.Split(','));
                    }
                    //Debug.Log(line);
                }
            }
        }

        Destroy(this.gameObject);
    }

}
