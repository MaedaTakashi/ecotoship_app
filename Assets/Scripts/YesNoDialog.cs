﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class YesNoDialog : MonoBehaviour,IPointerClickHandler {

    [SerializeField]
    private RectTransform Dialog;
    [SerializeField]
    private RectTransform BtnYes;
    [SerializeField]
    private RectTransform BtnNo;

    public delegate void DelegateProc();

    DelegateProc procYes;
    DelegateProc procNo;

    public void Show(string str, DelegateProc procYes, DelegateProc procNo)
    {
        Text txtMsg = Dialog.transform.Find("txt").GetComponent<Text>();
        txtMsg.text = str;

        this.procYes = procYes;
        this.procNo = procNo;
        this.gameObject.SetActive(true);

        StartCoroutine(sowButton());
    }

    IEnumerator sowButton()
    {
        yield return null;
        Vector2 size = Dialog.sizeDelta;
        float yesPosX = size.x / 4f;
        float btnPosY = -((size.y / 2f) - (BtnYes.sizeDelta.y/2f) - 30f);
        //Debug.Log("Dialog Size:" + size);
        BtnYes.anchoredPosition = new Vector2(-yesPosX, btnPosY);
        BtnNo.anchoredPosition = new Vector2( yesPosX, btnPosY);
        BtnYes.gameObject.SetActive(true);
        BtnNo.gameObject.SetActive(true);
    }

    public void ClickYes()
    {
        this.gameObject.SetActive(false);
        procYes();
    }

    public void ClickNo()
    {
        this.gameObject.SetActive(false);
        procNo();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        this.gameObject.SetActive(false);
        procNo();
        Debug.Log("click BG");
    }
}
